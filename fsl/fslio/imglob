#!/usr/bin/python
#   imglob - expand list of image filenames
#   Stephen Smith, Mark Jenkinson and Matthew Webster FMRIB Image Analysis Group
#   Copyright (C) 2009 University of Oxford 
#   SHCOPYRIGHT

import sys
import os
setAvailable=True
if sys.version_info < (2, 4):
   import sets
   from sets import Set
   setAvailable=False

def usage():
    print "Usage: $0 [-extension/extensions/missing] <list of names>"
    print "       -extension for one image with full extension"
    print "       -extensions for image list with full extensions"
    print "       -missing for a list of names with no corresponding file"
    sys.exit(1)

def removeExt(input):
    output=input.replace(".nii.gz","")    
    output=output.replace(".nii","")
    output=output.replace(".hdr.gz","")
    output=output.replace(".hdr","")
    output=output.replace(".img.gz","")
    output=output.replace(".img","")
    return output


if len(sys.argv) <= 1:
    usage()

deleteExtensions=True
singleImage=True;
findMissing=False;
startingArg=1

if sys.argv[1] == "-extensions":
   singleImage=False
   deleteExtensions=False
   startingArg=2
if sys.argv[1] == "-extension":
   deleteExtensions=False
   startingArg=2
if sys.argv[1] == "-missing":
   findMissing=True
   startingArg=2
    
filelist=[]
badlist=[]

for arg in range(startingArg, len(sys.argv)):
   output=removeExt(sys.argv[arg])
   if os.path.exists(output+".nii"):
      filelist.append(output+".nii")
   if os.path.exists(output+".nii.gz"):
      filelist.append(output+".nii.gz")
   if os.path.exists(output+".hdr"):
      filelist.append(output+".hdr")
   if os.path.exists(output+".hdr.gz"):
      filelist.append(output+".hdr.gz")
   if os.path.exists(output+".img.gz") and not singleImage:        
      filelist.append(output+".img.gz")
   if os.path.exists(output+".img") and not singleImage:        
      filelist.append(output+".img")
   if (not os.path.exists(output+".nii") and not os.path.exists(output+".nii.gz") and not os.path.exists(output+".hdr") and not os.path.exists(output+".hdr.gz") and not ( os.path.exists(output+".img.gz") and not singleImage ) and not ( os.path.exists(output+".img") and not singleImage )):
      badlist.append(output)

if findMissing:
   filelist=badlist

if deleteExtensions:
    for file in range(0, len(filelist)):
        filelist[file]=removeExt(filelist[file])
if setAvailable:
   filelist=list(set(filelist))
else:
   filelist=list(Set(filelist))
filelist.sort()


for file in range(0, len(filelist)):
    print filelist[file],
    if file < len(filelist)-1:
       print " ",

if len(badlist) > 0:
   sys.exit(1)
sys.exit(0)
