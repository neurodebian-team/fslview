/*  FSLView - 2D/3D Interactive Image Viewer

    Authors:    Rama Aravind Vorray
		James Saunders
		David Flitney 
		Mark Jenkinson
		Stephen Smith

    FMRIB Image Analysis Group

    Copyright (C) 2002-2005 University of Oxford  */

/*  CCOPYRIGHT */

#include <string>
#include <vector>
#include <boost/shared_ptr.hpp>

#include <QtCore/QSettings>
#include <QtCore/QRect>

class Preferences: public QSettings
{
public:
  typedef boost::shared_ptr<Preferences> Handle;

  std::string inqFSLDir();
  std::string inqMni152();
  std::string inqAssistantPath();
  std::string inqAtlasPath();
  QRect inqGeometry(int, int);

  std::vector<std::string> inqAtlasPathElements();

  void setFSLDir(const std::string&);
  void setMni152(const std::string&);
  void setAssistantPath(const std::string&);
  void setAtlasPath(const std::string&);
  void setGeometry(const QRect&);

  static Handle getInstance();

private:
  Preferences();

  static Handle m_instance;

  struct Implementation;  
  const std::auto_ptr< Implementation > m_impl;
};

