/*  FSLView - 2D/3D Interactive Image Viewer

    Authors:    Rama Aravind Vorray
		James Saunders
		David Flitney 
		Mark Jenkinson
		Stephen Smith

    FMRIB Image Analysis Group

    Copyright (C) 2002-2005 University of Oxford  */

/*  CCOPYRIGHT */

#include <QLabel>
#include <QGridLayout>
#include <QPixmap>

#include "sliceview.h"
#include "slicewidget.h"

std::string axisCodeToString(int code, bool lower)
{
  std::string s;
  switch(code)
    {
    case NIFTI_L2R: lower ? s = "L" : s = "R"; break;
    case NIFTI_R2L: lower ? s = "R" : s = "L"; break;
    case NIFTI_P2A: lower ? s = "P" : s = "A"; break;
    case NIFTI_A2P: lower ? s = "A" : s = "P"; break;
    case NIFTI_I2S: lower ? s = "I" : s = "S"; break;
    case NIFTI_S2I: lower ? s = "S" : s = "I"; break;
    default: s = ""; break;
    }

  return s;
}


SliceView::SliceView(QWidget* parent, const char *name):
  QWidget(parent, name)
{
  setupUi(this);
  setBackgroundColor(Qt::black);
//  setAutoFillBackground(true);
  //delete m_pixmapLabel;
}

void SliceView::setLabelsState(LabelState s)
{
  switch(s)
    {
    case Enabled:
      m_northLabel->setEnabled(true);
      m_southLabel->setEnabled(true);
      m_eastLabel->setEnabled(true);
      m_westLabel->setEnabled(true);
      break;
    case Greyed:
      m_northLabel->setEnabled(false);
      m_southLabel->setEnabled(false);
      m_eastLabel->setEnabled(false);
      m_westLabel->setEnabled(false);
      break;
    case Disabled:
      m_northLabel->setText("");
      m_southLabel->setText("");
      m_eastLabel->setText("");
      m_westLabel->setText("");
   default:
      break;
    }      
}

void SliceView::setSliceWidget(SliceWidget* slice)
{
  m_slice = slice;
  m_gridLayout->addWidget(slice, 1, 1);
  slice->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
}

QPixmap SliceView::getPixmap() const
{
  QPixmap pm(size());

  bitBlt(&pm,0,0,this);

  return pm;
}

void SliceView::setNorthText(const std::string& s)
{
  m_northLabel->setText(s.c_str());
}

void SliceView::setSouthText(const std::string& s)
{
  m_southLabel->setText(s.c_str());
}

void SliceView::setEastText(const std::string& s)
{
  m_eastLabel->setText(s.c_str());
}

void SliceView::setWestText(const std::string& s)
{
  m_westLabel->setText(s.c_str());
}
