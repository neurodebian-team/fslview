/*  FSLView - 2D/3D Interactive Image Viewer

    James Saunders, David Flitney and Stephen Smith, FMRIB Image Analysis Group

    Copyright (C) 2002-2012 University of Oxford  */

/*  CCOPYRIGHT */

#if !defined(LightboxWindow_H)
#define LightboxWindow_H

#include "imagewindow.h"
#include "slicewidget.h"
//Added by qt3to4:
#include <QResizeEvent>

class OverlayWidget;
class Q3ScrollView;
class QToolButton;

class LightboxWindow : public ImageWindow
{
  Q_OBJECT
public:
  LightboxWindow(QWidget *parent, ImageGroup::Handle i,
                 OverlayList::Handle ol, Cursor::Handle& c);
  virtual ~LightboxWindow();

//  virtual void update(const Cursor::Handle& c);

  virtual void resizeEvent(QResizeEvent*);

signals:

  void  volChanged(int); 

public slots:
  void scrolled(int);
  void repaintSlices();
  void setZoom(int);
  void print();

  virtual void on_actionSwitchViews_triggered();
private:
  void layoutSlices() const;

  SliceListHandle    m_slices;
  ImageGroup::Handle m_image;
  Q3ScrollView       *m_sv;
  QToolButton       *m_cursorModeButton;
  float              m_zoom;
};

#endif
