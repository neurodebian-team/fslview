/*  FSLView - 2D/3D Interactive Image Viewer

    James Saunders, David Flitney and Stephen Smith, FMRIB Image Analysis Group

    Copyright (C) 2002-2011 University of Oxford  */

/*  CCOPYRIGHT */

#if !defined(IMAGEWINDOW_H)
#define IMAGEWINDOW_H

#include <memory>

#include "imagewindowbase.h"

#include "imagegroup.h"
#include "overlaylist.h"
#include "cursor.h"
#include "drawsettings.h"
#include "viewoptions.h"
#include "slicewidget.h"
#include "shape.h"

class ImageWindow: public QMainWindow, public Ui::ImageWindow,
		   public CursorObserver, public OverlayListObserver
{
Q_OBJECT

public:
  typedef enum {Ortho = 0, Lightbox, Single} ViewStyle;
  
  ImageWindow(ImageGroup::Handle i,
	      OverlayList::Handle ol, 
	      Cursor::Handle c, 
	      QWidget *parent=0);
  
  ~ImageWindow();

  virtual void update(const OverlayList* i, OverlayListMsg msg);
  virtual void update(const Cursor::Handle& c);
  virtual void setLabels(const OverlayList*) {}

  virtual QSize sizeHint() const { return QSize(800,600); }

//  virtual OverlayList::Handle getOverlayList(){OverlayList::Handle h;return h;}
  OverlayList::Handle getOverlayList();

protected:
  void closeEvent(QCloseEvent *event);

private slots:
	virtual void on_actionSwitchViews_triggered() = 0;

	void on_actionBriconReset_triggered();

	void nextFrame();

	void setPenSize(int);
	void setPenColor(int);

	void on_actionToggleCrossHairs_toggled(bool);
	void on_actionResetZoom_triggered();
	void on_actionZoomMode_toggled(bool);
	void on_actionPanMode_toggled(bool);
	void on_actionCursorMode_toggled(bool);
	void on_actionOptions_triggered(bool);
	void on_actionSnapShot_triggered();

	virtual void on_actionMovieMode_toggled(bool);
	virtual void on_actionSliceRollMode_toggled(bool);

	// Mask editing actions
	void on_actionMaskEditMode_toggled(bool);

	void on_actionSelectPenTool_toggled(bool);
	void on_actionSelectEraseTool_toggled(bool);
	void on_actionSelectFillTool_toggled(bool);

	void on_actionRedo_triggered();
	void on_actionUndo_triggered();

	void on_actionLinkCursor_toggled(bool);

	void openOverlayDialog();

signals:
  void message(const QString&, int);
  void modeChanged(SliceWidget::Mode);
  void crossHairModeChanged(bool);
  void resetZoom();
  void windowClose(QCloseEvent*);

protected:
  Cursor::Handle getCursor();
  ViewOptions& getOpts();
  DrawSettings::Handle getDrawSettings();

  Cursor::Handle       m_globalCursor;
//  Cursor::Handle       m_cursor;
//  OverlayList::Handle  m_overlayList;
//  DrawSettings::Handle m_drawSettings;

  std::list<Shape::Handle>        m_undoList;
  std::list<Shape::Handle>        m_redoList;

//  ViewOptions         m_opts;

protected:
  struct Implementation;
  std::auto_ptr<Implementation> m_impl;
};

#endif
