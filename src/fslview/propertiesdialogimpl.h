#if !defined(PROPERTIESDIALOGIMPL_H)
#define PROPERTIESDIALOGIMPL_H

/* CCOPYRIGHT */

#include "propertiesdialog.h"

class PropertiesDialogImpl : public QDialog, private Ui::PropertiesDialog
{
public:
  static void getProperties(QWidget *);

private:
  void commit();

  PropertiesDialogImpl(QWidget *);
  ~PropertiesDialogImpl();
};
  
#endif
