/*  FSLView - 2D/3D Interactive Image Viewer

    Authors:    Rama Aravind Vorray
		James Saunders
		David Flitney 
		Mark Jenkinson
		Stephen Smith

    FMRIB Image Analysis Group

    Copyright (C) 2002-2003 University of Oxford  */

/*  CCOPYRIGHT */

#include "vtktoolbar.h"
#include "vtkwidget.h"

#include <qspinbox.h>

VTKToolbar::VTKToolbar(QWidget *parent, VTKProperties& p): 
  QWidget(parent), m_props(p) 
{
  setupUi(this);

  threshold->setValue(m_props.inqLowerThreshold());
}

void VTKToolbar::on_threshold_valueChanged(int v)
{
  cout << v << endl;
  m_props.setLowerThreshold(v);
}  

void VTKToolbar::on_clipping_stateChanged(int state)
{
  m_props.setClipping(state);
}
