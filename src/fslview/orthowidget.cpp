/*  FSLView - 2D/3D Interactive Image Viewer

    James Saunders, David Flitney and Stephen Smith, FMRIB Image Analysis Group

    Copyright (C) 2002-2003 University of Oxford  */

/*  CCOPYRIGHT */

#include "orthowidget.h"
#include <qlayout.h>
#include <qlabel.h>
#include <qlineedit.h>
#include <qwidget.h>
#include <qtimer.h>
#include <qpixmap.h>
#include <qimage.h>
#include <qspinbox.h>
#include <qstatusbar.h>
#include <qpushbutton.h>
#include <qtoolbar.h>
#include <qfiledialog.h>
#include <qtoolbutton.h>
#include <qlayout.h>
#include "tracker.h"
#include "ortho.xpm"
#include "overlaywidget.h"
#include "maintoolbar.h"
#include "modetoolbar.h"

#include <iostream>
using namespace std;

OrthoWidget::OrthoWidget(QWidget *parent, ImageGroup::Handle& i,
                         OverlayList::Handle ol, Cursor::Handle& c): 
  ImageWindow(i, ol, c, parent), m_image(i), m_layout(Traditional)
{
  TRACKER("OrthoWidget::OrthoWidget(QWidget *, Cursor*, ImageGroup::Handle&)");

//  m_centralWidget = new QWidget();
//
//  m_grid = new QGridLayout(m_centralWidget, 2, 2, 6, -1);
  //  m_centralWidget->setBackgroundColor(QColor(10,10,10));

  setIcon( QPixmap(ortho_xpm) );
  setWindowTitle("Orthographic View");
  //m_overlayList->attach(this);

  //m_slices = SliceListHandle(new SliceList);

  m_coronal  = new SliceView(m_centralWidget, "corornal");
  m_axial    = new SliceView(m_centralWidget, "axial");
  m_sagittal = new SliceView(m_centralWidget, "sagittal");
  setLayout(m_layout);

  SliceWidget* coronal =  new CoronalWidget(m_coronal, "coronal", getCursor(),
		  getOverlayList(), getDrawSettings(), m_undoList, getOpts());

  SliceWidget* axial =    new AxialWidget(m_axial, "axial", getCursor(),
		  getOverlayList(), getDrawSettings(), m_undoList, getOpts());

  SliceWidget* sagittal = new SagittalWidget(m_sagittal, "sagittal", getCursor(),
		  getOverlayList(), getDrawSettings(), m_undoList, getOpts());

  m_coronal->setSliceWidget(coronal);
  m_axial->setSliceWidget(axial);
  m_sagittal->setSliceWidget(sagittal);

  ImageInfo::Handle info(m_image->getMainImage()->getInfo());


  connect(this,     SIGNAL(modeChanged(SliceWidget::Mode)),
		  coronal,  SLOT(setMode(SliceWidget::Mode)));
  connect(this,     SIGNAL(modeChanged(SliceWidget::Mode)),
		  sagittal, SLOT(setMode(SliceWidget::Mode)));
  connect(this,     SIGNAL(modeChanged(SliceWidget::Mode)),
		  axial,    SLOT(setMode(SliceWidget::Mode)));

  // Add a zoom control and connect it to the slice widgets
  QSpinBox *zoomControl = new QSpinBox(this);
  zoomControl->setMaximum(1000);
  zoomControl->setMinimum(25);
  zoomControl->setValue(100);
  m_mainToolBar->insertWidget(actionCursorMode, zoomControl);
  connect(this, SIGNAL(resetZoom()), coronal, SLOT(resetZoom()));
  connect(this, SIGNAL(resetZoom()), sagittal,SLOT(resetZoom()));
  connect(this, SIGNAL(resetZoom()), axial,   SLOT(resetZoom()));

  connect(zoomControl, SIGNAL( valueChanged(int) ), coronal,  SLOT( setZoom(int) ));
  connect(zoomControl, SIGNAL( valueChanged(int) ), sagittal, SLOT( setZoom(int) ));
  connect(zoomControl, SIGNAL( valueChanged(int) ), axial,    SLOT( setZoom(int) ));
//
//  connect(coronal,  SIGNAL(message(const QString&, int )), SIGNAL(message(const QString&, int )));
//  connect(sagittal, SIGNAL(message(const QString&, int )), SIGNAL(message(const QString&, int )));
//  connect(axial,    SIGNAL(message(const QString&, int )), SIGNAL(message(const QString&, int )));
//
  connect(this, SIGNAL(crossHairModeChanged(bool)), coronal,  SLOT(crossHairMode(bool)));
  connect(this, SIGNAL(crossHairModeChanged(bool)), sagittal, SLOT(crossHairMode(bool)));
  connect(this, SIGNAL(crossHairModeChanged(bool)), axial,    SLOT(crossHairMode(bool)));
//
//  connect(coronal, SIGNAL(emitZoomFactor(int)), SLOT(setZoomValue(int)));
//  connect(sagittal,SIGNAL(emitZoomFactor(int)), SLOT(setZoomValue(int)));
//  connect(axial,   SIGNAL(emitZoomFactor(int)), SLOT(setZoomValue(int)));

  //m_cursor->attach(this);

  //std::for_each(m_slices->begin(), m_slices->end(), SetImageCursor(m_cursor));

  setLabels(getOverlayList().get());

//  setCentralWidget(m_centralWidget);
}

OrthoWidget::~OrthoWidget()
{
  TRACKER("OrthoWidget::~OrthoWidget");  
//  m_cursor->detach(this);
}

void OrthoWidget::print()
{
  QString fn = QFileDialog::getSaveFileName("screenshot.png", 
					    "PNG files (*.png)", this,
					    "Screenshot dialog",
					    "Select a filename for saving");
  if(!fn.isNull()) 
    {
//       QPixmap axial(m_axial->getPixmap());
//       QPixmap coronal(m_coronal->getPixmap());
//       QPixmap sagittal(m_sagittal->getPixmap());
  
//       int width  = sagittal.width() + coronal.width();
//       int height = axial.height() + coronal.height();
      
//       QPixmap composite(width, height );
//       composite.fill(QColor(128,128,128));

//       int ax(0), ay(coronal.height()), cx(0), cy(0), sx(axial.width()), sy(0);

//       copyBlt(&composite, ax, ay, &axial, 0, 0);
//       copyBlt(&composite, cx, cy, &coronal, 0, 0);
//       copyBlt(&composite, sx, sy, &sagittal, 0, 0);

      QPixmap pm(m_centralWidget->size());
      bitBlt(&pm, 0, 0, m_centralWidget);

//       QImage im = pm.convertToImage();
//       int dpm( (72.0 / 2.54) * 100.0 );
//       im.setDotsPerMeterX(dpm);
//       im.setDotsPerMeterY(dpm);
      pm.save(fn, "PNG", 100);
    }
}


void OrthoWidget::update(const OverlayList* o, OverlayListMsg msg)
{
  ImageWindow::update(o, msg);

  setLabels(o);
}

void OrthoWidget::setLayout(Layout l)
{
	m_layout = l;

	m_grid->removeWidget(m_coronal);
	m_grid->removeWidget(m_sagittal);
	m_grid->removeWidget(m_axial);

	switch(m_layout)
    {
    case Traditional:
		m_grid->addWidget(m_coronal,  0, 0);
		m_grid->addWidget(m_sagittal, 0, 1);
		m_grid->addWidget(m_axial,    1, 0);
		break;
    case InRow:
		m_grid->addWidget(m_sagittal, 0, 0);
		m_grid->addWidget(m_coronal,  0, 1);
		m_grid->addWidget(m_axial,    0, 2);
		break;
    case InColumn:
		m_grid->addWidget(m_sagittal, 0, 0);
		m_grid->addWidget(m_coronal,  1, 0);
		m_grid->addWidget(m_axial,    2, 0);
		break;
    default:
		break;
    }

	m_grid->activate();
}

void OrthoWidget::on_actionSwitchViews_triggered()
{
  m_layout = Layout( (m_layout+1) % (InColumn+1) );

  setLayout(m_layout);
}

//void OrthoWidget::changeView()
//{
//  m_layout = Layout( (m_layout+1) % (InColumn+1) );
//  setLayout();
//}

void OrthoWidget::setLabels(const OverlayList* o)
{
  // coronal=ik
  // axial=ij
  // sagittal=jk

  int icode(0), jcode(0), kcode(0);

  ImageInfo::Handle i(o->getActiveMetaImage()->getImage()->getInfo());
  
  i->inqAxisOrientations(icode, jcode, kcode);

  m_sagittal->setWestText(axisCodeToString(jcode, true));
  m_sagittal->setEastText(axisCodeToString(jcode, false));
  m_sagittal->setNorthText(axisCodeToString(kcode, false));
  m_sagittal->setSouthText(axisCodeToString(kcode, true));

  m_axial->setWestText(axisCodeToString(icode, i->isStoredRadiological()));
  m_axial->setEastText(axisCodeToString(icode, !i->isStoredRadiological()));
  m_axial->setNorthText(axisCodeToString(jcode, false));
  m_axial->setSouthText(axisCodeToString(jcode, true));

  m_coronal->setWestText(axisCodeToString(icode, i->isStoredRadiological()));
  m_coronal->setEastText(axisCodeToString(icode, !i->isStoredRadiological()));
  m_coronal->setNorthText(axisCodeToString(kcode, false));
  m_coronal->setSouthText(axisCodeToString(kcode, true));

  if(getOpts().inqShowLabels()) {
    if(i->hasValidXfms() ) {
      m_sagittal->setLabelsState(SliceView::Enabled);
      m_coronal->setLabelsState(SliceView::Enabled);
      m_axial->setLabelsState(SliceView::Enabled);
    } else {
      m_sagittal->setLabelsState(SliceView::Disabled);
      m_coronal->setLabelsState(SliceView::Disabled);
      m_axial->setLabelsState(SliceView::Disabled);
    }
  } else {
    m_sagittal->setLabelsState(SliceView::Disabled);
    m_coronal->setLabelsState(SliceView::Disabled);
    m_axial->setLabelsState(SliceView::Disabled);
  }
}
