/*  FSLView - 2D/3D Interactive Image Viewer

    Authors:    David Flitney 
		Mark Jenkinson
		Stephen Smith

    FMRIB Image Analysis Group

    Copyright (C) 2007 University of Oxford  */

/*  CCOPYRIGHT */

#include "atlasselectiondialog.h"

#include <q3listview.h>
#include <q3valuelist.h>
#include <qstringlist.h>
#include <q3header.h>
//Added by qt3to4:
#include <QPixmap>

#include "eye.xpm"

//#define DEBUGGING
#include "tracker.h"

struct SelectionListItem: public Q3CheckListItem
{
  SelectionListItem(Q3ListView* parent, const QString& text): 
    Q3CheckListItem(parent, text, CheckBox), m_preview(false) { refresh(); }

  void refresh();

  bool m_preview;
};

void SelectionListItem::refresh()
{
  if(m_preview)
    setPixmap(1, QPixmap(eye));
  else
    setPixmap(1, NULL);
}

struct AtlasSelectionDialog::Implementation
{
  Implementation(AtlasGroup::Handle ag):
    atlases(ag) { TRACKER("AtlasSelectionDialog::Implementation"); TRACE(); }
  ~Implementation() { TRACKER("AtlasSelectionDialog::~Implementation"); TRACE(); }

  void populateAtlasList(Q3ListView* l)
  {
    for(AtlasGroup::ConstIterator it = atlases->begin(); it != atlases->end(); ++it)
      parentList.append( new SelectionListItem(l, it->second->inqName().c_str()) );
  }

  Image::Handle probImage;
  AtlasGroup::Handle atlases;
  Q3ValueList<Q3ListViewItem *> parentList;
};

AtlasSelectionDialog::~AtlasSelectionDialog() {}

AtlasSelectionDialog::AtlasSelectionDialog(QWidget* p, const AtlasGroup::Handle ag):
  QDialog(p),
//   AtlasSelectionDialogBase(p, "AtlasSelectionDialog", true, 
// 			   WStyle_Customize|WStyle_DialogBorder), 
  m_impl(new Implementation(ag))
{
  setupUi(this);

  m_atlasList->clear();
}

void AtlasSelectionDialog::toggleDisplayAtlas(Q3ListViewItem* i)
{
  SelectionListItem *item = dynamic_cast<SelectionListItem*>(i);
  if(item) {
    item->m_preview ? item->m_preview = false : item->m_preview = true;
    item->refresh();
  }
}

bool AtlasSelectionDialog::showSummary(Atlas::Handle ah)
{
  SelectionListItem *item = 
    dynamic_cast<SelectionListItem*>( m_atlasList->findItem(ah->inqName().c_str(), 0) );
  if(item)
    return item->m_preview;
  else
    return false;
}

void AtlasSelectionDialog::enableAtlas(Atlas::Handle ah)
{
  if(ah)
    {
      SelectionListItem *i =
	dynamic_cast<SelectionListItem*>( m_atlasList->findItem(ah->inqName().c_str(), 0) );
      if(i) i->setOn(true);
    }
}

void AtlasSelectionDialog::populateAtlasList(AtlasGroup::Handle ag)
{
  m_impl->atlases=ag;
  m_impl->populateAtlasList(m_atlasList);
}

QStringList AtlasSelectionDialog::getSelectionList()
{
  QStringList atlasNames;
  Q3ListViewItemIterator it(m_atlasList, Q3ListViewItemIterator::Checked);
  while( it.current() ) {
    atlasNames.append(it.current()->text(0));
    ++it;
  }

  return atlasNames;
}

