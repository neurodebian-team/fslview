#include "propertiesdialogimpl.h"
#include "preferences.h"

#include "qlineedit.h"
#include "qtextedit.h"

PropertiesDialogImpl::PropertiesDialogImpl(QWidget *parent)
{
  setupUi(this);

  Preferences::Handle prefs=Preferences::getInstance();
  
  m_fslDir->setText(prefs->inqFSLDir().c_str());
  m_mniImage->setText(prefs->inqMni152().c_str());
  m_atlasPath->setText(prefs->inqAtlasPath().c_str());
  m_assistantPath->setText(prefs->inqAssistantPath().c_str());
}

void PropertiesDialogImpl::commit()
{
  Preferences::Handle prefs=Preferences::getInstance();
  
  prefs->setFSLDir(m_fslDir->text().toUtf8().constData());
  prefs->setMni152(m_mniImage->text().toUtf8().constData());
  prefs->setAssistantPath(m_assistantPath->text().toUtf8().constData());
  prefs->setAtlasPath(m_atlasPath->text().toUtf8().constData());
}

PropertiesDialogImpl::~PropertiesDialogImpl()
{
}

void PropertiesDialogImpl::getProperties(QWidget *parent)
{
  PropertiesDialogImpl propertiesDialog(parent);

  if(propertiesDialog.exec() == QDialog::Accepted)
    {
      propertiesDialog.commit();
    }
}
