//////////////////////////////////
// Copyright(c) Dave FLitney, 2011
//////////////////////////////////

#if !defined(MELVIEW_H)
#define MELVIEW_H

#include <QtGui>

#include <melviewbase.h>

class MelodicView: public QMainWindow, private Ui_MelViewBase
{
  Q_OBJECT

public:
  MelodicView(QWidget *parent=0);

// public slots:
//   void on_icTable_cellActivated(int, int);
};

class ICDelegate: public QStyledItemDelegate
{
  Q_OBJECT

public:
  ICDelegate(QObject *parent = 0) 
    : QStyledItemDelegate(parent) {}
  
  QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option,
			const QModelIndex &index) const;

  void setEditorData(QWidget *editor, const QModelIndex &index) const;
  void setModelData(QWidget *editor, 
		    QAbstractItemModel *model,
		    const QModelIndex &index) const;
  
  void updateEditorGeometry(QWidget *editor,
			    const QStyleOptionViewItem &option,
			    const QModelIndex &index) const;
  void paint(QPainter *painter, 
	     const QStyleOptionViewItem &option,
	     const QModelIndex &index) const;
};

// struct ICData
// {
//   QString classification;
//   QString filter;
  
//   ICData(QString c, QString f)
//     : classification(c), filter(f) {}

//   // void operator=(const ICData &d)
//   // {
//   //   classification = d.classification;
//   //   filter = d.filter;
//   // }
// };


#endif // MELVIEW_H
