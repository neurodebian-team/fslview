/*  FSLView - 2D/3D Interactive Image Viewer

    James Saunders, David Flitney and Stephen Smith, FMRIB Image Analysis Group

    Copyright (C) 2002-2003 University of Oxford  */

/*  CCOPYRIGHT */

#if !defined(SINGLEWIDGET_H)
#define SINGLEWIDGET_H

#include "imagewindow.h"
#include "slicewidget.h"

class QTimer;
class QToolButton;
class SliceView;

class SingleWidget : public ImageWindow
{
  Q_OBJECT
public:
  SingleWidget(QWidget *parent, ImageGroup::Handle i, 
              OverlayList::Handle ol,
              Cursor::Handle& c);
  virtual ~SingleWidget();
  //  virtual void update(const Cursor::Handle& c);

signals:
 
  void  volChanged(int);

private slots:
  void nextSlice();
  void setMovieFrameRate(int);
  void print();

  virtual void on_actionSwitchViews_triggered();

  virtual void on_actionSliceRollMode_toggled(bool);

private:
  void setLabels(const OverlayList*);
  void newSlice(int orient, int mode);
  SliceWidget::Handle  m_slice;
  SliceView 		  *m_view;
  ImageGroup::Handle   m_image;
  QTimer              *m_sliceRollTimer;  
  QToolButton         *m_cursorModeButton;
  int                  m_viewNumber;
};

#endif
