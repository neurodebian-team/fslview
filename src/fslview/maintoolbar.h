/*  FSLView - 2D/3D Interactive Image Viewer

    James Saunders, David Flitney and Stephen Smith, FMRIB Image Analysis Group

    Copyright (C) 2002-2003 University of Oxford  */

/*  CCOPYRIGHT */

#if !defined(MAINTOOLBAR_H)
#define MAINTOOLBAR_H

#include <qtoolbutton.h>
#include <qcheckbox.h>
#include <qspinbox.h>

#include "slicewidget.h"
#include "maintoolbarbase.h"

class MainToolBarWidget : public QWidget, private Ui::MainToolBarWidgetBase
{
  Q_OBJECT

public:
  MainToolBarWidget(QWidget *parent, int, int);
  virtual ~MainToolBarWidget();

  void enableMaskMode(bool on)      { maskModeButton->setEnabled(on);    }
 
  bool inqMaskMode()  { return maskModeButton->isOn();  }
  bool inqCrossHairState() { return crossHairsButton->isOn(); }

  void setCursorMode() { cursorModeButton->setOn(true); }
  void setPanMode()    { panModeButton->setOn(true);    }
  void setMaskMode()   { maskModeButton->setOn(true);   }
  void setZoomMode()   { zoomModeButton->setOn(true);   }
  void setCrossHairsMode(bool on);

public slots: 
  void setZoomValue(int);

private slots:
  void on_cursorModeButton_clicked(bool);
  void on_panModeButton_clicked(bool);
  void on_zoomModeButton_clicked(bool);
  void on_maskModeButton_clicked(bool);
  void on_crossHairsButton_clicked(bool);
  
signals:
  void modeChanged(SliceWidget::Mode);
  void zoomValueChanged(int);
  void crossHairStateChanged(bool);
  void resetZoomClicked();
};

#endif
