/*  FSLView - 2D/3D Interactive Image Viewer

    James Saunders, David Flitney and Stephen Smith, FMRIB Image Analysis Group

    Copyright (C) 2002-2003 University of Oxford  */

/*  CCOPYRIGHT */

#if defined(WIN32)
#pragma warning(disable:4786)
#endif

#include "singlewidget.h"
#include "sliceview.h"
//#include "maintoolbar.h"
//#include "modetoolbar.h"

#include <qcheckbox.h>
#include <qtimer.h>
#include <qtoolbar.h>
#include <qtoolbutton.h>
#include <qlayout.h>
#include "tracker.h"

// #include "sliceroll.xpm"
// #include "view.xpm"

// static const char * sliceRollText = "Slice roll mode.<br><hr>"
// "Click to automatically move through the slices.";
// static const char * viewText = "View Button.<br><hr>"
// "Press to move through axial, coronal and sagittal views.";

SingleWidget::SingleWidget(QWidget *parent, ImageGroup::Handle i,OverlayList::Handle ol, Cursor::Handle& c) :  
  ImageWindow(i,ol,c, parent), m_image(i), m_viewNumber(SliceWidget::Axial)
{
  TRACKER("SingleWidget::SingleWidget");

  setIcon( QPixmap(":/images/icons/single.xpm") );
  setWindowTitle("Single View");

  m_view = new SliceView(m_centralWidget, "view");
  m_grid->addWidget(m_view, 0, 0);

  newSlice(SliceWidget::Axial,SliceWidget::Cursing);


  ImageInfo::Handle info(m_image->getMainImage()->getInfo());

  actionSliceRollMode->setEnabled(true);

  m_sliceRollTimer = new QTimer( this );
  connect(m_sliceRollTimer, SIGNAL(timeout()), this, SLOT(nextSlice()));
//  connect(m_modeWidget, SIGNAL(sliceRollStateChanged(bool)),
//	  SLOT(toggleSliceRoll(bool)));

//  m_modeWidget->enableSwitchViews(true);
//  connect(m_modeWidget, SIGNAL(switchViewsClicked()), SLOT(changeView()));
}

SingleWidget::~SingleWidget()
{
  TRACKER("SingleWidget::~SingleWidget");
  m_sliceRollTimer->stop();

}

void SingleWidget::on_actionSliceRollMode_toggled(bool state)
{
  if(state)
    m_sliceRollTimer->start(getOpts().inqMovieFrameRate(), false);
  else
    m_sliceRollTimer->stop();
}

void SingleWidget::setMovieFrameRate(int ms)
{
//  ImageWindow::setMovieFrameRate(ms);
  if(m_sliceRollTimer->isActive())
    m_sliceRollTimer->changeInterval(ms);
}

// void SingleWidget::update(const Cursor::Handle& c)
// {
//   TRACKER("SingleWidget::update(Cursor)");
  
//   m_slice->setImageCursor(c->inqX(), c->inqY(), c->inqZ(), c->inqV());
// }

void SingleWidget::nextSlice()
{
	int n;
	Cursor::Handle cursor(getCursor());

	switch(m_slice->inqOrient())
	{
	case SliceWidget::Axial:
		n = (cursor->inqZ() + 1) % m_image->inqZ();
		cursor->setCursorRepaint(cursor->inqX(), cursor->inqY(), n);
		break;

	case SliceWidget::Coronal:
		n = (cursor->inqY() + 1) % m_image->inqY();
		cursor->setCursorRepaint(cursor->inqX(), n, cursor->inqZ());
		break;

	case SliceWidget::Sagittal:
		n = (cursor->inqX() + 1) % m_image->inqX();
		cursor->setCursorRepaint(n, cursor->inqY(),cursor->inqZ());
		break;
	}
}


void SingleWidget::on_actionSwitchViews_triggered()
{
  if(++m_viewNumber > 2) m_viewNumber = 0;
  newSlice(m_viewNumber, m_slice->inqMode());
}


void SingleWidget::newSlice(int orient, int mode)
{
	Cursor::Handle cursor(getCursor());

	if(orient == SliceWidget::Coronal)
		m_slice = SliceWidget::Handle(new CoronalWidget(m_view, "coronal", cursor,
				getOverlayList(), getDrawSettings(), m_undoList,
				getOpts()) );

	if(orient == SliceWidget::Sagittal)
		m_slice = SliceWidget::Handle(new SagittalWidget(m_view, "sagittal", cursor,
				getOverlayList(), getDrawSettings(), m_undoList,
				getOpts()) );

	if(orient == SliceWidget::Axial)
		m_slice = SliceWidget::Handle(new AxialWidget(m_view, "axial", cursor,
				getOverlayList(), getDrawSettings(), m_undoList,
				getOpts()) );

	m_view->setSliceWidget(m_slice.get());

//	connect(m_mainToolbarWidget, SIGNAL(modeChanged(SliceWidget::Mode)),
//			m_slice.get(), SLOT(setMode(SliceWidget::Mode)));
    connect(this, SIGNAL(crossHairModeChanged(bool)),     m_slice.get(), SLOT(crossHairMode(bool)));
    connect(this, SIGNAL(modeChanged(SliceWidget::Mode)), m_slice.get(), SLOT(setMode(SliceWidget::Mode)));
    connect(this, SIGNAL(crossHairModeChanged(bool)),     m_slice.get(), SLOT(crossHairMode(bool)));
    connect(this, SIGNAL(resetZoom()),                    m_slice.get(), SLOT(resetZoom()));

//    connect(this, SIGNAL(zoomValueChanged(int)),          m_slice.get(), SLOT(setZoom(int)));
//	connect(this, SIGNAL(resetZoom()),                    m_slice.get(), SLOT(resetZoom()));
//	connect(this, SIGNAL(crossHairModeChanged(bool)),     m_slice.get(), SLOT(crossHairMode(bool)));
//	connect(m_slice.get(),SIGNAL(message(const QString&, int )),
//			this ,SIGNAL(message(const QString&, int )));

//	m_slice->crossHairMode(m_mainToolbarWidget->inqCrossHairState());

	m_slice->setImageCursor(cursor->inqX(), cursor->inqY(), cursor->inqZ(), cursor->inqV());

	//  emit modeChanged(mode);

	m_slice->show();

	setLabels(getOverlayList().get());
}

void SingleWidget::setLabels(const OverlayList* o)
{
  int icode(0), jcode(0), kcode(0);

  ImageInfo::Handle i(o->getActiveMetaImage()->getImage()->getInfo());

  i->inqAxisOrientations(icode, jcode, kcode);

  switch(m_viewNumber)
  {
  case SliceWidget::Axial:
	  m_view->setWestText(axisCodeToString(icode, i->isStoredRadiological()));
	  m_view->setEastText(axisCodeToString(icode, !i->isStoredRadiological()));
	  m_view->setNorthText(axisCodeToString(jcode, false));
	  m_view->setSouthText(axisCodeToString(jcode, true));
	  break;
  case SliceWidget::Sagittal:
	  m_view->setWestText(axisCodeToString(jcode, true));
	  m_view->setEastText(axisCodeToString(jcode, false));
	  m_view->setNorthText(axisCodeToString(kcode, false));
	  m_view->setSouthText(axisCodeToString(kcode, true));
	  break;
  case SliceWidget::Coronal:
	  m_view->setWestText(axisCodeToString(icode, i->isStoredRadiological()));
	  m_view->setEastText(axisCodeToString(icode, !i->isStoredRadiological()));
	  m_view->setNorthText(axisCodeToString(kcode, false));
	  m_view->setSouthText(axisCodeToString(kcode, true));
  }
  if(getOpts().inqShowLabels()) {
	  if(i->hasValidXfms() ) {
		  m_view->setLabelsState(SliceView::Enabled);
	  } else {
		  m_view->setLabelsState(SliceView::Disabled);
	  }
  } else {
	  m_view->setLabelsState(SliceView::Disabled);
  }
}

#include <qfiledialog.h>
#include <qpixmap.h>

void SingleWidget::print()
{
  QString fn = QFileDialog::getSaveFileName("screenshot.png", 
					    "PNG files (*.png)", this,
					    "Screenshot dialog",
					    "Select a filename for saving");
  if(!fn.isNull()) 
    {
      QPixmap pm(centralWidget()->size());
      bitBlt(&pm, 0, 0, centralWidget());

//       QImage im = pm.convertToImage();
//       int dpm( (72.0 / 2.54) * 100.0 );
//       im.setDotsPerMeterX(dpm);
//       im.setDotsPerMeterY(dpm);
      pm.save(fn, "PNG", 100);
    }

}
