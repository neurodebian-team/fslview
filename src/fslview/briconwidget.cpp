/*  FSLView - 2D/3D Interactive Image Viewer

    James Saunders, David Flitney and Stephen Smith, FMRIB Image Analysis Group

    Copyright (C) 2002-2003 University of Oxford  */

/*  CCOPYRIGHT */


#include "briconwidget.h"

#include <qlineedit.h>
#include <qvalidator.h>
#include <qtoolbutton.h>
#include <qlayout.h>
#include <qpixmap.h>
#include <qlabel.h>
#include <qtooltip.h>

#include <qwt_wheel.h>

#include <algorithm>
#include <stdio.h>

//#define DEBUGGING
#include "tracker.h"

// class BriConWheel: public QwtWheel
// {
// public:
//   BriConWheel(QWidget *parent, const char *name = 0): QwtWheel(parent, name)
//   {
//     setOrientation(Qt::Horizontal);
//     setTickCnt(20);
//     setViewAngle(170);
//     setTotalAngle(6 * 360);
//   }
// };

// class LimitBox: public QLineEdit
// {
//  public:
//   LimitBox(QWidget *parent): QLineEdit(parent)
//   {
//     //    setFont(QFont("Helvetica", 10));
//     setMinimumWidth( QFontMetrics(font()).width(QString("-0.000000e-00")) );
//     setMaximumWidth( QFontMetrics(font()).width(QString("-0.000000e-00")) );
//     setValidator(new QDoubleValidator(this));
//     setAlignment(AlignLeft);
//   }
// };

BriConWidget::BriConWidget(QWidget* w, OverlayList::Handle list):
  QWidget(w),
  m_list(list)//, m_blockEvents(false)
{
	TRACKER("BriConWidget::BriconWidget");
  setupUi(this);

  MetaImage::Handle mi = m_list->getActiveMetaImage();
  if(!mi.get())
    mi = m_list->getMainMetaImage();
  m_bricon = mi->getDs()->inqBriCon();

  m_bricon->attach(this);

  m_list->attach(this);

  minBox->setText( QString("%1").arg(m_bricon->inqMin(),7,'g',5) );
//  QDoubleValidator *minv = new QDoubleValidator(minBox);
//  minv->setNotation(QDoubleValidator::ScientificNotation);
//  minBox->setValidator(minv);

  maxBox->setText( QString("%1").arg(m_bricon->inqMax(),7,'g',5) );
//  QDoubleValidator *maxv = new QDoubleValidator(minBox);
//  maxv->setNotation(QDoubleValidator::ScientificNotation);
//  maxBox->setValidator(maxv);

  briSlider->setRange(-49, 49, 2, 1);
  conSlider->setRange(-49, 49, 2, 1);

  updateMinMaxBoxes();
}

BriConWidget::~BriConWidget()
{
	TRACKER("BriConWidget::~BriconWidget");
  m_list->detach(this);
  m_bricon->detach(this);
}

void BriConWidget::setMinMaxBoxesState(bool state)
{
	TRACKER("BriConWidget::setMinMaxBoxesState(bool state)");
  minBox->setEnabled(state);
  maxBox->setEnabled(state);
}

void BriConWidget::setBriSliderState(bool state)
{
	TRACKER("BriConWidget::setBriSliderState(bool state)");
  briSlider->blockSignals(true);
  briSlider->setValid(state);
  briSlider->blockSignals(false);
}

void BriConWidget::setConSliderState(bool state)
{
	TRACKER("BriConWidget::setConSliderState(bool state)");
  conSlider->blockSignals(true);
  conSlider->setValid(state);
  conSlider->blockSignals(false);
}

void BriConWidget::reset()
{
	TRACKER("BriConWidget::reset()");
  m_bricon->detach(this);
  m_bricon->reset();
  m_bricon->attach(this);
  slidersChanged();
}

void BriConWidget::update(const BriCon* bricon)
{
  TRACKER("BriConWidget::update(const BriCon* bricon)");
  updateMinMaxBoxes();
}

void BriConWidget::update(const OverlayList* list, OverlayListMsg msg)
{
  TRACKER("BriConWidget::update(const OverlayList* list, OverlayListMsg msg)");

  if(OverlayListMsg(Select) == msg) 
    {
      MESSAGE("Select");
      MetaImage::Handle mi = list->getActiveMetaImage();
      if(!mi.get())
        mi = list->getMainMetaImage();
      m_bricon->detach(this);
      m_bricon = mi->getDs()->inqBriCon();
      m_bricon->attach(this);
      minBox->setText( QString("%1").arg(m_bricon->inqMin(),7,'g',5) );
      maxBox->setText( QString("%1").arg(m_bricon->inqMax(),7,'g',5) );
    }
  if((OverlayListMsg(Select) == msg) || (OverlayListMsg(Visibility) == msg))
    {
      MESSAGE("Select || Visibility");
      bool state(list->getActiveMetaImage()->inqVisibility());
      setMinMaxBoxesState(state);
      setBriSliderState(state);
      setConSliderState(state);
    }
}

void BriConWidget::minChanged()
{
  TRACKER("BriConWidget::minChanged");

  float value = minBox->text().toFloat();
  m_bricon->setMin(value);
}

void BriConWidget::maxChanged()
{
  TRACKER("BriConWidget::maxChanged");

  float value = maxBox->text().toFloat();
  m_bricon->setMax(value);
}

void BriConWidget::on_briSlider_valueChanged(double value)
{
  TRACKER("BriConWidget::briSliderChanged");

  m_bricon->modifyRange(value / 100.0, 0.0);
  updateMinMaxBoxes();
}

void BriConWidget::on_conSlider_valueChanged(double value)
{
  TRACKER("BriConWidget::conSliderChanged");

  m_bricon->modifyRange(0.0, value / 100.0);
  updateMinMaxBoxes();
}

void BriConWidget::slidersChanged()
{
  TRACKER("BriConWidget::slidersChanged");

  m_bricon->updateRange();

  minBox->setText( QString("%1").arg(m_bricon->inqMin(),7,'g',5) );
  maxBox->setText( QString("%1").arg(m_bricon->inqMax(),7,'g',5) );
  
  briSlider->setValue(0);
  conSlider->setValue(0);
}

void BriConWidget::updateMinMaxBoxes()
{
  TRACKER("BriConWidget::updateMinMaxBoxes()");
  minBox->blockSignals(true);
  minBox->setText( QString("%1").arg(m_bricon->inqAdjustedMin(),7,'g',5) );
  minBox->blockSignals(false);

  maxBox->blockSignals(true);
  maxBox->setText( QString("%1").arg(m_bricon->inqAdjustedMax(),7,'g',5) );
  maxBox->blockSignals(false);
}
