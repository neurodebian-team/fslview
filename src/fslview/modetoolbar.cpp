/*  FSLView - 2D/3D Interactive Image Viewer

    James Saunders, David Flitney and Stephen Smith, FMRIB Image Analysis Group

    Copyright (C) 2002-2003 University of Oxford  */

/*  CCOPYRIGHT */

#include "modetoolbar.h"

#include <QToolTip>

using namespace std;

ModeToolBarWidget::ModeToolBarWidget(QWidget *parent)
  : QWidget(parent)
{
  setupUi(this);

  connect(m_movieModeButton,   SIGNAL(clicked(bool)), SIGNAL(movieStateChanged(bool)));
  connect(m_sliceRollButton,   SIGNAL(clicked(bool)), SIGNAL(sliceRollStateChanged(bool)));
  connect(m_switchViewsButton, SIGNAL(clicked()),     SIGNAL(switchViewsClicked()));
  connect(m_optionsButton,     SIGNAL(clicked()),     SIGNAL(optionsClicked()));
  connect(m_printButton,       SIGNAL(clicked()),     SIGNAL(printClicked()));
}

ModeToolBarWidget::~ModeToolBarWidget()
{
}

void
ModeToolBarWidget::setSwitchHelpText(const string& s)
{
  QToolTip::add(m_switchViewsButton, s.c_str());
}

