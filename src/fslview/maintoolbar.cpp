/*  FSLView - 2D/3D Interactive Image Viewer

    James Saunders, David Flitney and Stephen Smith, FMRIB Image Analysis Group

    Copyright (C) 2002-2003 University of Oxford  */

/*  CCOPYRIGHT */

#include <qcheckbox.h>
#include <qspinbox.h>
#include "maintoolbar.h"

MainToolBarWidget::MainToolBarWidget(QWidget *parent, int min, int max): 
  QWidget(parent)
{
  setupUi(this);

  jumpToMaxButton->hide();
  connect(zoomSpinBox,       SIGNAL(valueChanged(int)), SIGNAL(zoomValueChanged(int)));
  connect(crossHairsButton,  SIGNAL(clicked(bool)),     SIGNAL(crossHairStateChanged(bool)));
  connect(viewResetButton,   SIGNAL(clicked()),         SIGNAL(resetZoomClicked()));
}

MainToolBarWidget::~MainToolBarWidget()
{
}

void MainToolBarWidget::setCrossHairsMode(bool checked)
{ 
  crossHairsButton->blockSignals(true); 
  crossHairsButton->setOn(checked); 
  crossHairsButton->blockSignals(false); 
}

void MainToolBarWidget::setZoomValue(int f)
{
  zoomSpinBox->blockSignals(true);
  zoomSpinBox->setValue(f);
  zoomSpinBox->blockSignals(false);
}

void MainToolBarWidget::on_crossHairsButton_clicked(bool y)
{
    emit crossHairStateChanged(y);
}

void MainToolBarWidget::on_panModeButton_clicked(bool y)
{
  if(y)
    emit modeChanged(SliceWidget::Pan);    
}

void MainToolBarWidget::on_cursorModeButton_clicked(bool y)
{ 
  if(y)
    emit modeChanged(SliceWidget::Cursing); 
}

void MainToolBarWidget::on_zoomModeButton_clicked(bool y)
{ 
  if(y)
    emit modeChanged(SliceWidget::Zoom);
}

void MainToolBarWidget::on_maskModeButton_clicked(bool y)
{ 
  if(y)
    emit modeChanged(SliceWidget::Masking); 
}

