fslview for Debian
------------------

FSLView can make use of brain atlases to provide information about stereotaxic
coordinates. While FSL provides nine different atlases that can be used with
fslview (and atlasquery), these atlases are covered by a non-commercial
license and are not part of Debian.

However, as the format of the atlases is documented in the HTML documentation
of FSLView it is easy the produce new atlases (at least technically).

Additionally users may download the FSL source tarball from

    http://www.fmrib.ox.ac.uk/fsl/

and extract the contained 'fsl/data/atlases' subdirectory that contains the
above mentioned atlases. Atlases should be put into /usr/share/fsl/data/atlases.

There is also an unofficial Debian package of the atlases and other datasets
available. For convenience the fslview package recommends the unofficial atlas
package although it is not part of Debian.

For the time being unofficial FSL binary and data packages are available from:

    http://neuro.debian.net/pkgs/fsl.html

 -- Michael Hanke <michael.hanke@gmail.com>  Wed, 30 Sep 2009 21:34:23 +0200
